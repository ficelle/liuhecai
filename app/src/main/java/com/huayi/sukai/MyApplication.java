package com.huayi.sukai;

import com.iflytek.cloud.SpeechUtility;

import io.dcloud.application.DCloudApplication;

/**
 * Created by zyh on 2017/4/17.
 */

public class MyApplication extends DCloudApplication {
    @Override
    public void onCreate() {
        super.onCreate();
        SpeechUtility.createUtility(MyApplication.this, "appid=58f2365b");
    }
}
